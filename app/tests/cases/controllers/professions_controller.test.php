<?php
/* Professions Test cases generated on: 2010-08-26 19:08:57 : 1282865877*/
App::import('Controller', 'Professions');

class TestProfessionsController extends ProfessionsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ProfessionsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.profession', 'app.user', 'app.professions_user');

	function startTest() {
		$this->Professions =& new TestProfessionsController();
		$this->Professions->constructClasses();
	}

	function endTest() {
		unset($this->Professions);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

}
?>