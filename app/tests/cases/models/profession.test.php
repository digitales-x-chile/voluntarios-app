<?php
/* Profession Test cases generated on: 2010-08-21 14:08:30 : 1282413930*/
App::import('Model', 'Profession');

class ProfessionTestCase extends CakeTestCase {
	var $fixtures = array('app.profession', 'app.user', 'app.professions_user');

	function startTest() {
		$this->Profession =& ClassRegistry::init('Profession');
	}

	function endTest() {
		unset($this->Profession);
		ClassRegistry::flush();
	}

}
?>