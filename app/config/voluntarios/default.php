<?php
$config['Voluntarios']['name'] = 'Panel de Voluntarios'; // Nombre de la Aplicación
$config['Voluntarios']['siteUrl'] = 'http://www.example.com'; // URL del sitio web de la organización
$config['Voluntarios']['appUrl'] = 'http://app.example.com';  // URL de la aplicación de Voluntarios
$config['Voluntarios']['salt'] = 'UnaSal_';  // Sal para los Passwords

$config['Voluntarios']['mail']['replyTo'] = 'contact@example.com';
$config['Voluntarios']['mail']['from'] = 'Nuestra Organización <no-reply@example.com>';
$config['Voluntarios']['mail']['subject'] = 'Bienvenido a Nuestra Organización';
$config['Voluntarios']['mail']['recoverSubject'] = 'Nuestra Organización - Recuperación de Contraseña';

$config['Voluntarios']['style']['theme'] = ''; // Define el nombre del theme que quieres utilizar para tu aplicación, si es necesario
?>
