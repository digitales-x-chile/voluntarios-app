<?php

/** Rutas de Usuario **/

Router::connect('/', array('controller' => 'users', 'action' => 'add')); // Página de Inicio users->add
Router::connect('/login', array('controller' => 'users', 'action' => 'login')); // Páginas de registro users->login

/** Rutas de Administración **/

Router::connect('/admin', array('controller' => 'users', 'action' => 'index', 'admin' => true)); // Página de Inicio Administración users->index

/** Rutas de Voluntario **/

Router::connect('/users', array('controller' => 'users', 'action' => 'me'));