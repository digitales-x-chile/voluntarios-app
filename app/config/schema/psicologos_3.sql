RENAME TABLE
	`professions`
		TO `knowledge_areas`,
	`professions_users`
		TO `user_knowledges`;
	
ALTER TABLE
	`knowledge_areas`
		ADD  `verified` TINYINT( 1 ) NOT NULL DEFAULT '0';

ALTER TABLE
	`user_knowledges`
		CHANGE `profession_id` `knowledge_area_id` INT( 11 ) NOT NULL;

ALTER TABLE
	`user_knowledges`
		ADD  `knowledge_level_id` TINYINT NOT NULL DEFAULT '1' ,
		ADD  `name` VARCHAR( 255 ) NOT NULL ,		
		ADD  `validated` TINYINT( 1 ) NOT NULL DEFAULT  '0' ,
		ADD  `file` VARCHAR( 255 ) NOT NULL ,
		ADD  `reference` TEXT NOT NULL;

CREATE TABLE 
	`knowledge_levels` (
		`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`name` VARCHAR( 255 ) NOT NULL ,
		`order` INT NOT NULL ,
		`validate`TINYINT ( 1 ) NOT NULL DEFAULT '0'
	) ENGINE = INNODB;

INSERT INTO
	`knowledge_levels` (`id` ,`name` ,`order`) VALUES (NULL ,  'Default',  '0');

UPDATE
	`dbversion` set version=3;