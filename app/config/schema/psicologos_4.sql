ALTER TABLE `users`
	ADD  `location_id` INT NULL DEFAULT NULL AFTER  `secondary_lastname` ,
	ADD  `city` VARCHAR( 200 ) NULL DEFAULT NULL AFTER  `location_id` ,
	ADD  `address` VARCHAR( 500 ) NULL DEFAULT NULL AFTER  `city` ,
	ADD  `phone` VARCHAR( 20 ) NULL DEFAULT NULL AFTER  `address` ,
	ADD  `curriculum` VARCHAR( 500 ) NULL DEFAULT NULL AFTER  `phone`;

CREATE TABLE  `locations` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	`parent_id` INT NOT NULL ,
	`lft` INT DEFAULT NULL,
	`rght` INT DEFAULT NULL,
	`name` VARCHAR( 100 ) NOT NULL
) ENGINE = INNODB;

UPDATE
	`dbversion` set version=4;