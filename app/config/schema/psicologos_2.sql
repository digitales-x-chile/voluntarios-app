ALTER TABLE `users` MODIFY status ENUM('unverified','active','locked') NOT NULL default 'unverified';
UPDATE `dbversion` set version=2;