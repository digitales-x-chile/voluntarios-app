ALTER TABLE  `users`
	ADD  `curriculum_dir` VARCHAR( 255 ) NULL AFTER  `curriculum` ,
	ADD  `curriculum_mimetype` VARCHAR( 255 ) NULL AFTER  `curriculum_dir` ,
	ADD  `curriculum_filesize` INT UNSIGNED NULL AFTER  `curriculum_mimetype`;

UPDATE
	`dbversion` set version=5;