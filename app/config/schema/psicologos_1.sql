ALTER TABLE `users` ADD COLUMN status ENUM('unverified','active') NOT NULL default 'unverified';
ALTER TABLE `users` ADD COLUMN admin boolean NOT NULL default false;
UPDATE `dbversion` set version=1;
