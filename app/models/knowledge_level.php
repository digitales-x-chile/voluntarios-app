<?php
class KnowledgeLevel extends AppModel {
	var $name = 'KnowledgeLevel';
	var $displayField = 'name';
	var $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'El nombre del Nivel de Conocimiento es obligatorio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	
	var $order = array(
		'KnowledgeLevel.order ASC',
		'KnowledgeLevel.name ASC',
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'UserKnowledge' => array(
			'className' => 'UserKnowledge',
			'foreignKey' => 'knowledge_level_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>