<?php
class User extends AppModel {
	var $name = 'User';
	var $validate = array(
		'mail' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'Debes ingresar una dirección de correo válido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'isUnique' => array(
				'rule' => array('isUnique'),
				'message' => 'Este e-mail ya se encuentra registrado.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'id_number' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debes ingresar un número de identificación.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'rut' => array(
				'rule' => array('validarRut'),
				'message' => 'Debes ingresar un RUT válido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'rutUnique' => array(
				'rule' => array('isUnique'),
				'message' => 'El RUT ingresado ya está registrado en nuestra base de datos.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'id_type' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'firstname' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar tu nombre',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'lastname' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debes ingresar tu apellido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'password' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debes ingresar una contraseña para inscribirte.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
	        'identicalFieldValues' => array(
				'rule' => array('identicalFieldValues', 'password_check' ),
				'message' => 'Tu password no coincide con la confirmación'
			) 
		),
		'password_check' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debes ingresar la confirmación de tu password',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'location_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debes seleccionar una localidad',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'curriculum' => array(
			'Empty' => array(
				'check' => false,
			),
		),
	);
	
	var $actsAs = array(
			'MeioUpload.MeioUpload' => array(
				'curriculum' => array(
					'allowedMime' => array('application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.oasis.opendocument.text'),
					'allowedExt' => array('.pdf','.doc','.docx','.odt'),
					'fields' => array(
						'dir' => 'curriculum_dir',
						'filesize' => 'curriculum_filesize',
						'mimetype' => 'curriculum_mimetype'
					),
				),
			),
		);
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'UserKnowledge' => array(
			'className' => 'UserKnowledge',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
	);
	
	var $belongsTo = array(
		'Location' => array(
			'className' => 'Location',
			'foreignKey' => 'location_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	var $virtualFields = array(
    	'name' => 'CONCAT(firstname, " ", lastname)',
    	'fullname' => 'CONCAT(firstname, " ", lastname, " ", secondary_lastname)'
	);
	
	var $displayField = 'name';
	
	function identicalFieldValues( $field=array(), $compare_field=null ) 
    {
        foreach( $field as $key => $value ){
            $v1 = $value;
            $v2 = $this->data[$this->name][ $compare_field ];                 
            if($v1 !== $v2) {
                return FALSE;
            } else {
                continue;
            }
        }
        return true;
    }
    
    function sanitizarRut($rut){
    	$temp = explode('-',$rut);
		if (!isset($temp[1]) || $temp[1] == ''){
			$temp[0] = substr($rut,0,strlen($rut)-1);
			$temp[1] = substr($rut,strlen($rut)-1);		
		}
		$digito_v = $temp[1];
		$digito_v = ($digito_v=='k')?'K':$digito_v;
	
		$rut = str_replace(array('.',',',' '),'',$temp[0]);

		if(!is_numeric($rut)) $rut = 0;

		return number_format($rut, 0, ',', '.').'-'.$digito_v;
    }
	
	function validarRut($rut){
	
		$rut = $rut['id_number'];
		
		$temp = explode('-',$rut);

		$digito_v = $temp[1];
		
		$rut = str_replace(array('.',',',' '),'',$temp[0]);

		if(!$rut>0)
			return false;
					
		$temp = 2;
		$suma = 0;

		while ($rut > 0){
			$digito = ($rut % 10);
			$suma += $digito * $temp;
			$temp = ($temp==7) ? 2 : ($temp+1);
			$rut = intval($rut/10);
		}
		
		$dv = 11 - $suma % 11;
		$dv = ($dv==11)?0:$dv;
		$dv = ($dv==10)?'K':$dv;
		
		$count = 0;
		
		if((string)$digito_v == (string)$dv)
			return true;
		else
			return false;
	}
	
	function beforeValidate(){
    
    	/** Sinitiza y Valida el RUT si es que el tipo de ID seleccionado e Carnet de Identidad **/
    	if($this->data){
    		if($this->data['User']['id_type'] == 'Carnet de Identidad'){
	    		$this->data['User']['id_number'] = $this->sanitizarRut($this->data['User']['id_number']);
    		}else{
    			unset($this->validate['id_number']['rut']);
	    	}
    	
    		if(isset($this->data['User']['forceDuplicateIdNumber']) && $this->data['User']['forceDuplicateIdNumber']){
    			unset($this->validate['id_number']['rutUnique']);
	    	}
    	}
    	
    	return true;
    }

	function hashPasswords($data, $enforce=false){
		if($enforce && isset($this->data[$this->alias]['password'])) {
			if(!empty($this->data[$this->alias]['password'])) {
				$this->data[$this->alias]['password'] = Security::hash($this->data[$this->alias]['password'], null, true);
			}
		}

		return $data;
	}
	
	function beforeSave() {
         $this->hashPasswords(null, true);
         return true;
    }

}
?>