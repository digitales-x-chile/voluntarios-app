<?php
class UserKnowledge extends AppModel {
	var $name = 'UserKnowledge';
	var $validate = array(
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'file' => array(
			'Empty' => array(
				'check' => false,
			),
		),
		'name' => array(
			'text' => array(
				'rule' => array('notEmpty'),
				'message' => 'Este campo es obligatorio',
			)
		),
	);	
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	
	var $displayField = 'name';
	
	var $actsAs = array(
			'MeioUpload.MeioUpload' => array(
				'file' => array(
					'allowedMime' => array('application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.oasis.opendocument.text'),
					'allowedExt' => array('.pdf','.doc','.docx','.odt'),
				),
			),
		);

	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'KnowledgeArea' => array(
			'className' => 'KnowledgeArea',
			'foreignKey' => 'knowledge_area_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'KnowledgeLevel' => array(
			'className' => 'KnowledgeLevel',
			'foreignKey' => 'knowledge_level_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>