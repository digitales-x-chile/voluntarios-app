<?php
class AppController extends Controller {

	var $components = array('Auth','Session');
	var $helpers = array(
		'Session',
		'Html',
		'Form',
		'Js' => array(
			'Jquery',
		),
		'Text',
	);
	
	var $view = 'Theme';
	
	function beforeFilter() {
		$this->Auth->fields = array(
			'username' => 'mail', 
			'password' => 'password'
		);
		
		 if (isset($this->params['prefix']) && $this->params['prefix'] == 'admin') {
			if($this->Session->read('Auth.User.admin') != true){
				$this->Session->setFlash('No tienes permisos para acceder al panel de administración');
				$this->redirect(array('controller' => 'users', 'action' => 'login', 'admin' => false));
			}
			$this->Auth->loginRedirect = array('controller' => 'users', 'action' => 'index', 'admin' => true);
		}
			
		$this->Auth->loginRedirect = array('controller' => 'users', 'action' => 'index');		
		$this->Auth->userScope = array('User.status' => 'active');
    	$this->Auth->loginError = "Tu e-mail o contraseña son incorrectos";
    	$this->Auth->authError = "No tienes autorización para ingresar aquí";
		$this->Auth->logoutRedirect = array(Configure::read('Routing.admin') => false, 'controller' => 'users', 'action' => 'add');
	}
	
	
	function beforeRender () {
		$this->_setErrorLayout();
		$theme = Configure::read('Voluntarios.style.theme');
		if(!empty($theme)){
			$this->theme = $theme;
		}
	}

	function _setErrorLayout() {
    	if($this->name == 'CakeError') {
	        $this->layout = 'error';
    	}
	}

	
	
}