<?php
class KnowledgeLevelsController extends AppController {

	var $name = 'KnowledgeLevels';

	function admin_index() {
		$this->KnowledgeLevel->recursive = 0;
		$this->set('knowledgeLevels', $this->paginate());
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->KnowledgeLevel->create();
			if ($this->KnowledgeLevel->save($this->data)) {
				$this->Session->setFlash(__('El Nivel de Conocimiento ha sido guardado.', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El Nivel de Conocimiento no ha sido guardado. Inténtalo nuevamente.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('El Nivel de Conocimiento es inválido.', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->KnowledgeLevel->save($this->data)) {
				$this->Session->setFlash(__('El Nivel de Conocimiento ha sido guardado.', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El Nivel de Conocimiento no ha sido guardado. Inténtalo nuevamente.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->KnowledgeLevel->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('El Nivel de Conocimiento es inválido.', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->KnowledgeLevel->delete($id)) {
			$this->Session->setFlash(__('El Nivel de Conocimiento ha sido eliminado.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('El Nivel de Conocimiento no ha sido eliminado.', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>