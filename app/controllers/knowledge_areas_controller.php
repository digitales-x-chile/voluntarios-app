<?php
class KnowledgeAreasController extends AppController {

	var $name = 'KnowledgeAreas';

	function admin_index() {
		$this->KnowledgeArea->recursive = 0;
		$this->set('knowledge_areas', $this->paginate());
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->KnowledgeArea->create();
			if ($this->KnowledgeArea->save($this->data)) {
				$this->Session->setFlash(__('El Área de Conocimiento ha sido añadida', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El Área de Conocimiento no ha podido ser añadida. Inténtalo nuevamente.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Área de Conocimiento Inválida', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->KnowledgeArea->save($this->data)) {
				$this->Session->setFlash(__('El Área de Conocimiento ha sido modificada', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El Área de Conocimiento no ha podido ser modificado. Inténtalo nuevamente.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->KnowledgeArea->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('El ID del Área de Conocimiento es inválido.', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->KnowledgeArea->delete($id)) {
			$this->Session->setFlash(__('El Área de Conocimiento ha sido eliminada.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('El Área de Conocimiento no ha podido ser eliminada.', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>