<?php
class UserKnowledgesController extends AppController {

	var $name = 'UserKnowledges';
		
	function admin_index(){
		$this->paginate = array(
			'conditions' => array(
				'UserKnowledge.validated = 0',
			),
			'recursive' => 1,
		);
		$this->set('userKnowledges', $this->paginate());
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->UserKnowledge->create();
			if ($this->UserKnowledge->save($this->data)) {
				$this->Session->setFlash(__('El Área de Conocimiento del Usuario ha sido guardado.', true));
				$this->redirect(array('controller' => 'users', 'action' => 'view', $this->data['UserKnowledge']['user_id']));
			} else {
				$this->Session->setFlash(__('El Área de Conocimiento del Usuario no ha sido guardado. Inténtalo nuevamente.', true));
			}
		}
		
		if(isset($this->params['named']['user_id'])){
			$this->set('user_id', $this->params['named']['user_id']);
			$this->data['UserKnowledge']['user_id'] = $this->params['named']['user_id'];
		}
		
		$users = $this->UserKnowledge->User->find('list');
		$knowledgeAreas = $this->UserKnowledge->KnowledgeArea->find('list');
		$knowledgeLevels = $this->UserKnowledge->KnowledgeLevel->find('list');
		$this->set(compact('users', 'knowledgeAreas', 'knowledgeLevels'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('El Área de Conocimiento del Usuario es Inválido.', true));
			$this->redirect($this->referer());
		}
		if (!empty($this->data)) {
			if ($this->UserKnowledge->save($this->data)) {
				$this->Session->setFlash(__('El Área de Conocimiento del Usuario ha sido guardado.', true));
				$this->redirect(array('controller' => 'users', 'action' => 'view', $this->data['UserKnowledge']['user_id']));
			} else {
				$this->Session->setFlash(__('El Área de Conocimiento del Usuario no ha sido guardado. Inténtalo nuevamente.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->UserKnowledge->read(null, $id);
		}
		$this->set('user_id', $this->data['UserKnowledge']['user_id']);
		$users = $this->UserKnowledge->User->find('list');
		$knowledgeAreas = $this->UserKnowledge->KnowledgeArea->find('list');
		$knowledgeLevels = $this->UserKnowledge->KnowledgeLevel->find('list');
		$this->set(compact('users', 'knowledgeAreas', 'knowledgeLevels'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('El Área de Conocimiento del Usuario es Inválido.', true));
			$this->redirect($this->referer());
		}
		if ($this->UserKnowledge->delete($id)) {
			$this->Session->setFlash(__('El Área de Conocimiento del Usuario ha sido eliminado.', true));
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(__('El Área de Conocimiento del Usuario no ha sido eliminado. Inténtalo nuevamente', true));
		$this->redirect($this->referer());
	}
	
	function admin_validation($id) {
		if (!$id) {
			$this->Session->setFlash(__('El Área de Conocimiento del Usuario es Inválido.', true));
			$this->redirect($this->referer());
		}
		
		if(!isset($this->params['named']['opt'])){
			$this->params['named']['opt'] = false;
		}else{
			$this->params['named']['opt'] = true;
		}
		
		$this->UserKnowledge->read(array('id'), $id);
		$this->UserKnowledge->set('validated', $this->params['named']['opt']);
		if($this->UserKnowledge->save()) {
			$this->Session->setFlash(__('El Área de Conocimiento del Usuario ha sido modificada.', true));
		}else{
			$this->Session->setFlash(__('El Área de Conocimiento del Usuario no ha sido modificada.', true));
		}
		
		$this->redirect($this->referer());
		
	}
	
	/** Funciones de Usuario normal **/
	
	function add() {
	
		$this->set('user_id', $this->Session->read('Auth.User.id'));
	
		if (!empty($this->data)) {
			$this->UserKnowledge->create();
			
			$knowledge_level = $this->UserKnowledge->KnowledgeLevel->read(null, $this->data['UserKnowledge']['knowledge_level_id']);

			if($knowledge_level['KnowledgeLevel']['validate']){
				$this->data['UserKnowledge']['validated'] = false;
			}else{
				$this->data['UserKnowledge']['validated'] = true;
			}
			
			$this->data['UserKnowledge']['user_id'] = $this->Session->read('Auth.User.id');

			if ($this->UserKnowledge->save($this->data)) {
				$this->Session->setFlash(__('El Área de Conocimiento del Usuario ha sido guardado.', true));
				$this->redirect(array('controller' => 'users', 'action' => 'me'));
			} else {
				$this->Session->setFlash(__('El Área de Conocimiento del Usuario no ha sido guardado. Inténtalo nuevamente.', true));
			}
		}

		$knowledgeAreas = $this->UserKnowledge->KnowledgeArea->find('list');
		$knowledgeLevels = $this->UserKnowledge->KnowledgeLevel->find('list');
		$this->set(compact('knowledgeAreas', 'knowledgeLevels'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('El Área de Conocimiento del Usuario es Inválido.', true));
			$this->redirect(array('controller' => 'users', 'action' => 'me'));
		}
		
		$user_knowledge = $this->UserKnowledge->read(null, $id);
		
		if($user_knowledge['UserKnowledge']['user_id'] != $this->Session->read('Auth.User.id')){
			$this->Session->setFlash(__('No estás autorizado para realizar esta acción.', true));
			$this->redirect(array('controller' => 'users', 'action' => 'me'));
		}
		
		if($user_knowledge['UserKnowledge']['validated']){
			$this->Session->setFlash(__('Esta Área de Conocimiento ya ha sido validada por lo que no puedes ya modificarla.', true));
			$this->redirect(array('controller' => 'users', 'action' => 'me'));
		}
		
		if (!empty($this->data)) {
			unset($this->data['UserKnowledge']['validated']);
			if ($this->UserKnowledge->save($this->data)) {
				$this->Session->setFlash(__('El Área de Conocimiento del Usuario ha sido guardado.', true));
				$this->redirect(array('controller' => 'users', 'action' => 'me'));
			} else {
				$this->Session->setFlash(__('El Área de Conocimiento del Usuario no ha sido guardado. Inténtalo nuevamente.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->UserKnowledge->read(null, $id);
		}
		$this->set('user_id', $this->data['UserKnowledge']['user_id']);
		$users = $this->UserKnowledge->User->find('list');
		$knowledgeAreas = $this->UserKnowledge->KnowledgeArea->find('list');
		$knowledgeLevels = $this->UserKnowledge->KnowledgeLevel->find('list');
		$this->set(compact('users', 'knowledgeAreas', 'knowledgeLevels'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('El Área de Conocimiento del Usuario es Inválido.', true));
			$this->redirect($this->referer());
		}
		
		$user_knowledge = $this->UserKnowledge->read(null, $id);
		
		if($user_knowledge['UserKnowledge']['user_id'] != $this->Session->read('Auth.User.id')){
			$this->Session->setFlash(__('No estás autorizado para realizar esta acción.', true));
			$this->redirect(array('controller' => 'users', 'action' => 'me'));
		}
		
		if ($this->UserKnowledge->delete($id)) {
			$this->Session->setFlash(__('El Área de Conocimiento del Usuario ha sido eliminado.', true));
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(__('El Área de Conocimiento del Usuario no ha sido eliminado. Inténtalo nuevamente', true));
		$this->redirect($this->referer());
	}
	
	
}
?>