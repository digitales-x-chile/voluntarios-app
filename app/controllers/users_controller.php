<?php
class UsersController extends AppController {

	var $name = 'Users';
	var $components = array('Email');
	var $helpers = array('Csv');

	/* function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	} */

	/* function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid user', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('user', $this->User->read(null, $id));
	} */
	
	function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('login','add','confirmed','confirmation','recover');
		if(in_array($this->action, array('add','recover','edit', 'password', 'admin_edit', 'admin_add'))){
	    	$this->Auth->authenticate = $this->User;
	    }
	}
	
	function login() {
		$this->layout = 'auth';	
    }

    function logout() {
        $this->redirect($this->Auth->logout());
    }
    
	function add() {
		if($this->Session->read('Auth.User.id')){
			$this->Session->setFlash('Ya te encuentras registrado.');
			$this->redirect(array('controller' => 'users', 'action' => 'index'));
		}
		if (!empty($this->data)) {
		
			//debug($this->data); exit;
			$this->User->create();
			
			if($this->data['User']['id_type'] == 'Carnet de Identidad'){
				$this->data['User']['id_number'] = $this->User->sanitizarRut($this->data['User']['id_number']);
			}
					
			if ($this->User->save($this->data)) {
				
				$this->set('host',Configure::read('HOST'));
				$this->set('name', $this->data['User']['firstname']);
				$this->set('mail', $this->data['User']['mail']);
				$this->set('id', $this->User->id);
				$this->set('hash', md5(Configure::read('Voluntarios.salt').$this->data['User']['mail']));
				
    			$this->Email->to = $this->data['User']['firstname'].' <'.$this->data['User']['mail'].'>';
				$this->Email->subject = Configure::read('Voluntarios.mail.subject');
				$this->Email->replyTo = Configure::read('Voluntarios.mail.replyTo'); // Verificar y Cambiar
				$this->Email->from = Configure::read('Voluntarios.mail.from');
				$this->Email->template = 'add';
				$this->Email->sendAs = 'text';
				
				$this->Email->send();
    		
				$this->Session->setFlash(__('Gracias por registrarte. Hemos enviado un e-mail de confirmación a tu cuenta de correo.', true));
				$this->redirect(array('action' => 'confirmation'));

				
			} else {
				$this->Session->setFlash(__('Ha ocurrido un error, Por favor, inténtalo nuevamente.', true));
			}
		}
		$user_knowledges = $this->User->UserKnowledge->find('list',array('order' => array('UserKnowledge.name ASC')));
		$this->set('idTypes',array('Carnet de Identidad'=>'Carnet de Identidad','Pasaporte'=>'Pasaporte'));
		$this->set(compact('user_knowledges'));
		
		$errors = $this->User->invalidFields();
		
		if( isset($errors['id_number']) && $errors['id_number'] == $this->User->validate['id_number']['rutUnique']['message'] ){
			$this->set('displayForceIdNumber',true);
		}
		
		$this->layout = 'register';
		
	}
	
	function confirmation(){
	
		if(isset($this->params['named']['hash']) && isset($this->params['named']['id'])){
	
			$hash = $this->params['named']['hash'];
			$id = $this->params['named']['id'];
			
			if($user = $this->User->read(null,$id)){
				if(md5(Configure::read('Voluntarios.salt').$user['User']['mail']) == $hash){
					$this->User->set('status','active');
					$this->User->query("UPDATE users SET status = 'active' WHERE id = $id", false);
					//$this->User->save(); // Refactorizar. $Model->save no me está funcionando.
					//$this->User->saveAll($user); // 
					$this->redirect(array('action' => 'confirmed'));
				}else{
					exit;
				}
			}else{
				exit;
			}
			
		}
		
		$this->layout = 'auth';
		
	}
	
	function confirmed(){
	
		$this->layout = 'auth';
	}
	
	function recover(){
		
		if($this->data){
		
			if($user = $this->User->findByMail($this->data['User']['mail'])){
				$password = substr(md5(uniqid(mt_rand(), true)), 0, 8);
				$user['User']['password'] = $password;
				
				if($this->User->save($user, false)){
		
					$this->set('name', $user['User']['name']);
					$this->set('password', $password);
					$this->set('mail', $user['User']['mail']);
					
					$this->Email->to = $this->data['User']['name'].' <'.$this->data['User']['mail'].'>';
					$this->Email->subject = Configure::read('Voluntarios.mail.recoverSubject');
					$this->Email->replyTo = Configure::read('Voluntarios.mail.replyTo');
					$this->Email->from = Configure::read('Voluntarios.mail.from');
					$this->Email->template = 'pass_recover';
					$this->Email->sendAs = 'text';
					
					$this->Email->send();
	    		
	    			$this->Session->setFlash(__('Te hemos enviado un e-mail a ', true).$this->data['User']['mail'].' con una nueva contraseña.');
	    			$this->redirect(array('controller' => 'users', 'action' => 'login'));
    			
    			}else{
    			
    				$this->Session->setFlash(__('Ha ocurrido un error. Pro favor inténtalo nuevamente',true));
    			
    			}
				
				
				
			}else{
				$this->Session->setFlash(__('El e-mail ingresado no existe en nuestro sistema',true), 'default' , array() , 'auth');
			}
		}
			
		$this->layout = 'auth';
	
	}

	function me(){
		$args = array(
			'conditions' => array(
				'User.id' => $this->Session->read('Auth.User.id'),
			),
			'recursive' => 2,
		);
		
		$user = $this->User->find('first', $args);
		
		$user['Location'] = $this->User->Location->getpath( $user['User']['location_id'] );
		
		$this->set('user', $user);
		
	}

	function edit() {

		if (!empty($this->data)) {

			if($this->data['User']['id_type'] == 'Carnet de Identidad'){
				$this->data['User']['id_number'] = $this->User->sanitizarRut($this->data['User']['id_number']);
			}

			if(empty($this->data['User']['password'])){
				unset($this->data['User']['password']);
				unset($this->data['User']['password_check']);
			}

			$options = array(
				'fieldList' => array(
					'firstname',
					'lastname',
					'secondary_lastname',
					'mail',
					'password',
					'location_id',
					'city',
					'address',
					'phone',
				),
			);

			if ($this->User->save($this->data, $options)) {
				$this->Session->setFlash(__('Tu información ha sido modificada.', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Tu información no ha sido modificada. Inténtalo nuevamente.', true));

				$errors = $this->User->invalidFields();

				if( isset($errors['id_number']) && $errors['id_number'] == $this->User->validate['id_number']['rutUnique']['message']){
					$this->set('displayForceIdNumber',true);
				}
			}

		}

		if (empty($this->data)) {
			$this->data = $this->User->read(null, $this->Session->read('Auth.User.id'));
			unset($this->data['User']['password']);
		}
		
		$this->set('locations', $this->User->Location->generatetreelist(null, null,null,'- '));

		$this->set('idTypes',array('Carnet de Identidad'=>'Carnet de Identidad','Pasaporte'=>'Pasaporte'));
		$user_knowledges = $this->User->UserKnowledge->find('list');
		$this->set(compact('user_knowledges'));

	}
	
	function edit_curriculum(){
		if($this->data){
			$this->data['User']['id'] = $this->Session->read('Auth.User.id');
			if($this->User->saveAll($this->data)){
				$this->Session->setFlash('Tu currículum ha sido modificado');
			}else{
				$this->Session->setFlash('Ha ocurrido un error. Intenta modificar tu currículum nuevamente');
			}
		}
		
		$this->redirect(array('action' => 'index'));
	}
	
	function admin_index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

	function admin_export(){
		$this->User->recursive = 1;
		$this->set('users', $this->User->find('all'));
		$this->layout = 'ajax';
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid user', true));
			$this->redirect(array('action' => 'index'));
		}
		
		$args = array(
			'conditions' => array(
				'User.id' => $id,
			),
			'recursive' => 2,
		);
		
		
		$user = $this->User->find('first', $args);
		
		$user['Location'] = $this->User->Location->getpath( $user['User']['location_id'] );
		
		$this->set('user', $user);
		
		
	}
	
	function admin_login() {
		$this->layout = 'auth';
	}
	
	function admin_logout() {
        $this->redirect($this->Auth->logout());
    }

	function admin_add() {
		if (!empty($this->data)) {
			$this->User->create();
			if($this->data['User']['id_type'] == 'Carnet de Identidad'){
				$this->data['User']['id_number'] = $this->User->sanitizarRut($this->data['User']['id_number']);
			}
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('El voluntario ha sido añadido', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El voluntario no se ha añadido. Inténtalo nuevamente.', true));
			}
		}
		$this->set('idTypes',array('Carnet de Identidad'=>'Carnet de Identidad','Pasaporte'=>'Pasaporte'));
		$user_knowledges = $this->User->UserKnowledge->find('list');
		$this->set(compact('user_knowledges'));

		$errors = $this->User->invalidFields();

		if( isset($errors['id_number']) && $errors['id_number'] == $this->User->validate['id_number']['rutUnique']['message'] ){
			$this->set('displayForceIdNumber',true);
		}
		
		$this->set('locations', $this->User->Location->generatetreelist(null, null,null,'- '));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Usuario inválido', true));
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->data)) {

			if($this->data['User']['id_type'] == 'Carnet de Identidad'){
				$this->data['User']['id_number'] = $this->User->sanitizarRut($this->data['User']['id_number']);
			}

			if(empty($this->data['User']['password'])){
				unset($this->data['User']['password']);
				unset($this->data['User']['password_check']);
			}
			
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('El usuario ha sido modificado', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El usuario no ha sido modificado. Inténtalo nuevamente.', true));

				$errors = $this->User->invalidFields();

				if( isset($errors['id_number']) && $errors['id_number'] == $this->User->validate['id_number']['rutUnique']['message']){
					$this->set('displayForceIdNumber',true);
				}
			}

		}

		if (empty($this->data)) {
			$this->data = $this->User->read(null, $id);
			unset($this->data['User']['password']);
		}

		$this->set('user', array('User' => array('id' => $id, 'name' => $this->data['User']['firstname'].' '.$this->data['User']['lastname'])));

		$this->set('idTypes',array('Carnet de Identidad'=>'Carnet de Identidad','Pasaporte'=>'Pasaporte'));
		$user_knowledges = $this->User->UserKnowledge->find('list');

		$statuses = array(
			'unverified' => __('Sin Verificar', true),
			'active' => __('Activo', true),
			'locked' => __('Desactivado', true),
		);

		$this->set(compact('user_knowledges', 'statuses'));
		
		$this->set('locations', $this->User->Location->generatetreelist(null, null,null,'- '));

	}
	
	function admin_edit_curriculum(){		
		if($this->data){
			if($this->User->saveAll($this->data)){
				$this->Session->setFlash('El currículum ha sido modificado');
				$this->redirect(array('action' => 'view', $this->data['User']['id']));
			}else{
				$this->Session->setFlash('Ha ocurrido un error. Intenta modificar el currículum nuevamente');
			}
		}
		
		$this->redirect(array('action' => 'index'));
	}

	function admin_lock($id = null) {
			$user = $this->User->read(null, $id);

			$this->User->id = $user['User']['id'];
			$this->User->set('status', 'locked');

			if($this->User->save(null, array('validate' => false, 'fieldList' => array('status')))){
				$this->Session->setFlash(__('El usuario ha sido desactivado', true));
			}else{
				$this->Session->setFlash(__('El usuario no ha podido ser desactivado'));
			}

			$this->redirect($this->referer());

	}

	function admin_unlock($id = null) {
			$user = $this->User->read(null, $id);

			$this->User->id = $user['User']['id'];
			$this->User->set('status', 'active');

			if($this->User->save(null, array('validate' => false, 'fieldList' => array('status')))){
				$this->Session->setFlash(__('El usuario ha sido activado', true));
			}else{
				$this->Session->setFlash(__('El usuario no ha podido ser activado'));
			}

			$this->redirect($this->referer());

	}


	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('ID de usuario inválido', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->User->delete($id)) {
			$this->Session->setFlash(__('Usuario eliminado', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__(' El usuarios no ha sido eliminado', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>