<?php
class LocationsController extends AppController {

	var $name = 'Locations';

	function admin_index() {
		$this->Location->recursive = 0;
		$this->paginate['Location'] = array( 
	        'order' => array('Location.lft asc'),
		);
		$this->set('locations', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid location', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('location', $this->Location->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Location->create();
			if ($this->Location->save($this->data)) {
				$this->Session->setFlash(__('The location has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The location could not be saved. Please, try again.', true));
			}
		}		
		$this->set('locations', $this->Location->generatetreelist(null, null,null,'- '));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid location', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Location->save($this->data)) {
				$this->Session->setFlash(__('The location has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The location could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Location->read(null, $id);
		}
		$this->set('locations', $this->Location->generatetreelist(null, null,null,'- '));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for location', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Location->delete($id)) {
			$this->Session->setFlash(__('Location deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Location was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	
	function admin_move_up($id){
		if (!$id) {
			$this->redirect(array('action'=>'index'));
		}
		
		$this->Location->id = $id;
		$this->Location->moveUp($id, 1);
		$this->redirect(array('action'=>'index')); 
	}
	
	function admin_move_down($id){
		if (!$id) {
			$this->redirect(array('action'=>'index'));
		}
		
		$this->Location->id = $id;
		$this->Location->moveDown($id, 1);
		$this->redirect(array('action'=>'index', 'scope_id' => $scope_id )); 
	}
}
?>