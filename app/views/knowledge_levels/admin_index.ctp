<div class="knowledgeLevels index">
	<h2><?php __('Niveles de Conocimiento');?></h2>
	
	<ul class="viewTools">
			<li><?php echo $this->Html->link('añadir', array('action' => 'add'), array('class' => 'button add')); ?></li>
	</ul>
	
	
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('Nombre', 'name');?></th>
			<th><?php echo $this->Paginator->sort('Orden', 'order');?></th>
			<th><?php echo $this->Paginator->sort('Validación', 'validate');?></th>
			<th class="actions"><?php __('Acciones');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($knowledgeLevels as $knowledgeLevel):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><strong><?php echo $knowledgeLevel['KnowledgeLevel']['name']; ?></strong></td>
		<td><?php echo $knowledgeLevel['KnowledgeLevel']['order']; ?></td>
		<td><?php echo ($knowledgeLevel['KnowledgeLevel']['validate'])?'Sí Requiere Validación':'No Requiere Validación'; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('modificar', true), array('action' => 'edit', $knowledgeLevel['KnowledgeLevel']['id']), array('class' => 'button edit')); ?>
			<?php echo $this->Html->link(__('eliminar', true), array('action' => 'delete', $knowledgeLevel['KnowledgeLevel']['id']), array('class' => 'button delete'), sprintf(__('¿Estás seguro que quieres eliminar %s? Recuerda que todos los conocimientos de los usuarios que pertenezcan a este nivel también serán eliminados.', true), $knowledgeLevel['KnowledgeLevel']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, mostrando %current% registros de un total de %count%, comenzando en el registro %start%, terminando en el %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('siguiente', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>