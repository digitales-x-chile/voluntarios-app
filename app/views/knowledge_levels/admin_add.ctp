<div class="knowledgeLevels form">
	<h2><?php __('Nuevo Nivel de Conocimiento');?></h2>

	<ul class="viewTools">
			<li><?php echo $this->Html->link('ver niveles', array('action' => 'index'), array('class' => 'button index')); ?></li>
	</ul>
	
<?php echo $this->Form->create('KnowledgeLevel');?>
	<fieldset>
 		<legend><?php __('Nuevo Nivel de Conocimiento'); ?></legend>
	<?php
		echo $this->Form->input('name', array('label' => 'Nombre'));
		echo $this->Form->input('order', array('label' => 'Orden'));
		echo $this->Form->input('validate', array('label' => 'Requiere Validación por parte del Usuario'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Añadir', true));?>
</div>