<div class="knowledgeAreas index">
	<h2><?php __('Áreas de Conocimiento');?></h2>

	<ul class="viewTools">
		<li id="knowledgeAreasNewWrap" class="viewTools">
			<?php echo $this->Html->link(__('añadir',true), array('action' => 'add'), array('class' => 'button add', 'id' => 'addKnowledgeAreaInPlace')); ?>

			<div id="knowledgeAreasNew" class="form">
				<fieldset>
					<legend>Añadir Área de Conocimiento</legend>
					<?php
						echo $this->Form->create('KnowledgeArea', array( 'url' => array('action' => 'add')));
						echo $this->Form->input('name', array('type' => 'text', 'label' => __('Nombre', true)));
						echo $this->Form->input('verified', array('label' => 'Verificada', 'checked' => 'checked'));
						echo $this->Form->end(__('Añadir', true));
					?>
				</fieldset>
			</div>
		</li>
	</ul>
		<script type="text/javascript">

			$(document).ready(function(){
				$("#addKnowledgeAreaInPlace").click(function(){
					if($("#knowledgeAreasNew").hasClass('open')){					
						$("#knowledgeAreasNew").slideUp('fast');
					}else{
						$("#knowledgeAreasNew").slideDown('fast');
					}
					$("#knowledgeAreaInPlace").toggleClass('open');
					$("#knowledgeAreasNew").toggleClass('open');
					return false;
				});
			});

		</script>


	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('Nombre', 'name');?></th>
			<th><?php echo $this->Paginator->sort('Verificación', 'verified');?></th>
			<th class="actions">&nbsp;</th>
	</tr>
	<?php
	$i = 0;
	foreach ($knowledge_areas as $knowledge_area):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $knowledge_area['KnowledgeArea']['name']; ?>&nbsp;</td>
		<td><?php echo $this->Html->image('icons/knowledge_area_status_'.$knowledge_area['KnowledgeArea']['verified'].'.png'); ?></td>
		<td class="actions">
			<?php echo $this->Html->link(__('modificar', true), array('action' => 'edit', $knowledge_area['KnowledgeArea']['id']), array('class' => 'button edit')); ?>
			<?php echo $this->Html->link(__('eliminar', true), array('action' => 'delete', $knowledge_area['KnowledgeArea']['id']), array('class' => 'button delete'), sprintf(__('¿Estás seguro que quieres eliminar "%s"?', true), $knowledge_area['KnowledgeArea']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>