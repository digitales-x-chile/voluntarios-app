<div class="knowledgeAreas form">

	<h2>Modificar Área de Conocimiento</h2>
	<ul class="viewTools">
			<li><?php echo $this->Html->link(__('ver áreas de conocimiento', true), array('action' => 'index'), array('class' => 'button index')); ?></li>
	</ul>


<?php echo $this->Form->create('KnowledgeArea');?>
	<fieldset>
 		<legend><?php __('Modificar Área de Conocimiento'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name', array('type' => 'text', 'label' => 'Nombre'));
		echo $this->Form->input('verified', array('label' => 'Verificada'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Modificar', true));?>
</div>