<?php

$line = array(
	'status' => 'Status',
	'firstname' => 'Nombre',
	'lastname' => 'Apellido',
	'secondary_lastname' => 'Apellido 2',
	'mail' => 'E-mail',
	'id_type' => 'Tipo ID',
	'id_number' => 'Nº Identificación',
	'region' => 'Región',
	'comuna' => 'Comuna',
	'city' => 'Ciudad',
	'address' => 'Dirección',
	'telefono' => 'Teléfono',
	'curriculum' => 'Currículum',
	'area_id' => ' ID Area de Conocimiento',
	'area' => 'Area de Conocimiento',
	'area_file' => 'Archivo de Validacion',
	'area_reference' => 'Referencia',
);

$this->Csv->addRow($line);

foreach($users as $user):

	$line = array(
		'status' => $user['User']['status'],
		'firstname' => $user['User']['firstname'],
		'lastname' => $user['User']['lastname'],
		'secondary_lastname' => $user['User']['secondary_lastname'],
		'mail' => $user['User']['mail'],
		'id_type' => $user['User']['id_type'],
		'id_number' => $user['User']['id_number'],
		'region' => null,
		'comuna' => $user['Location']['name'],
		'city' => $user['User']['city'],
		'address' => $user['User']['address'],
		'telefono' => $user['User']['phone'],
		'curriculum' => (!empty($user['User']['curriculum']))?$this->Html->url(DS.$user['User']['curriculum_dir'].DS.$user['User']['curriculum'], true):'',
		'area_id' => null,
		'area' => null,
		'area_file' => null,
		'area_reference' => null,
	);

	if(isset($user['UserKnowledge']) && count($user['UserKnowledge']) > 0){
		foreach($user['UserKnowledge'] as $profession){
			$line['area_id'] = $profession['id'];
			$line['area'] = $profession['name'];
			$line['area_file'] = (!empty($profession['file']))?$this->Html->url(DS.'uploads/user_knowledge/file'.DS.$profession['file'], true):'';
			$line['area_reference'] = $profession['reference'];
			$this->Csv->addRow($line);
		}
	}else{
		$this->Csv->addRow($line);
	}

endforeach;

echo $this->Csv->render('Voluntarios.csv');

?>