<div class="users index">
	<h2><?php __('Voluntarios');?></h2>
	<ul class="viewTools">
			<li><?php echo $this->Html->link('Nuevo', array('action' => 'add'), array('class' => 'button add')); ?></li>
			<li><?php echo $this->Html->link('Descargar CSV', array('action' => 'export'), array('class' => 'button download')); ?></li>
			<li><?php echo $this->Html->link('Validación Areas de Conocimiento', array('controller' => 'user_knowledges', 'action' => 'index'), array('class' => 'button userknowledgevalid')); ?></li>
	</ul>


	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('St', 'status');?></th>
		<th><?php echo $this->Paginator->sort('Nombre', 'name');?></th>
		<th><?php echo $this->Paginator->sort('E-mail', 'mail');?></th>
		<th><?php echo $this->Paginator->sort('Nº de Identificación', 'id_number');?></th>
		<th><?php echo $this->Paginator->sort('Registrado', 'created');?></th>
		<th class="actions">&nbsp;</th>
	</tr>
	<?php
	$i = 0;
	foreach ($users as $user):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $this->Html->image('icons/user_status_'.$user['User']['status'].'.png', array('alt' => $user['User']['status'])); ?></td>
		<td>
			<?php echo ($user['User']['admin'])?$this->Html->image('icons/user_admin.png', array('alt' => 'Administrador')):''; ?>
			<?php echo $this->Html->link($user['User']['name'], array('action' => 'view', $user['User']['id'])); ?>
		</td>
		<td><?php echo $this->Html->link($user['User']['mail'], 'mailto:'.$user['User']['mail']); ?> </td>
		<td>
			<small><?php echo $user['User']['id_type']; ?></small><br/>
			<?php echo $user['User']['id_number']; ?>&nbsp;
		</td>
		<td><?php echo $user['User']['created']; ?>&nbsp;</td>
		<td class="actions">
			<?php switch($user['User']['status']){
				case 'active':
					echo $this->Html->link(__('desactivar', true), array('action' => 'lock', $user['User']['id']), array('class' => 'button userlock'));
					break;
				case 'locked':
				case 'unverified':
					echo $this->Html->link(__('activar', true), array('action' => 'unlock', $user['User']['id']), array('class' => 'button userunlock'));
					break;
			}
			?>
			<?php echo $this->Html->link(__('modificar', true), array('action' => 'edit', $user['User']['id']), array('class' => 'button edit')); ?>
			<?php echo $this->Html->link(__('eliminar', true), array('action' => 'delete', $user['User']['id']), array('class' => 'button delete'), sprintf(__('¿Estás seguro que quieres eliminar a "%s"?', true), $user['User']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, mostrando %current% del total de %count%, desde el %start% hasta el %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>