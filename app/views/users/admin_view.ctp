<div class="users view userSection">

	<h2>Voluntario <strong><?php echo $user['User']['name']; ?></strong></h2>
	<ul class="viewTools">
			<li><?php echo $this->Html->link('ver voluntarios', array('action' => 'index'), array('class' => 'button index')); ?></li>
			<li>
			<?php switch($user['User']['status']){
				case 'active':
					echo $this->Html->link(__('desactivar', true), array('action' => 'lock', $user['User']['id']), array('class' => 'button userlock'));
					break;
				case 'locked':
				case 'unverified':
					echo $this->Html->link(__('activar', true), array('action' => 'unlock', $user['User']['id']), array('class' => 'button userunlock'));
					break;
			}
			?>
			</li>
			<li><?php echo $this->Html->link('modificar', array('action' => 'edit', $user['User']['id']), array('class' => 'button edit')); ?></li>
	</ul>
	

	<dl><?php $i = 0; $class = ' class="altrow"';?>

		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Nombre'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $user['User']['firstname']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Apellido'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $user['User']['lastname']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Segundo Apellido'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $user['User']['secondary_lastname']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Nº de Identificación'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<small><?php echo $user['User']['id_type']; ?></small>
			<?php echo $user['User']['id_number']; ?>
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Registrado'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $user['User']['created']; ?>
			&nbsp;
		</dd>
	</dl>
	
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('E-mail'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($user['User']['mail'], 'mailto:'.$user['User']['mail']); ?>
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Localidad'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php $locations = array(); foreach($user['Location'] as $location) $locations[] = $location['Location']['name']; ?>
			<?php echo implode(' › ', $locations); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Ciudad'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $user['User']['city']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Address'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $user['User']['address']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Teléfono'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $user['User']['phone']; ?>
			&nbsp;
		</dd>
	</dl>

</div>

<div class="userSection">
	<h3><?php __('Información Profesional y de Conocimientos');?></h3>
	<ul class="viewTools">
			<li><?php echo $this->Html->link('añadir área', array('controller' => 'user_knowledges', 'action' => 'add', 'user_id' => $user['User']['id']), array('class' => 'button add')); ?></li>
			<li>
			<?php echo $this->Html->link('modificar currículum', array('action' => 'edit_curriculum', $user['User']['id']), array('class' => 'button add', 'id' => 'addUserCurriculumInPlace')); ?>
			<div id="userCurriculumAdd" class="form">
				<fieldset>
					<legend>Modificar Curriculum</legend>
					<?php echo $this->Form->create('User', array('type' => 'file', 'url' => array('action' => 'edit_curriculum', $user['User']['id']))); ?>
					<?php echo $this->Form->input('User.id', array('type' => 'hidden', 'value' => $user['User']['id'])); ?>
					<?php echo $this->Form->input('User.curriculum', array('type' => 'file', 'label' => __('Selecciona un Archivo', true))); ?>
					<?php echo $this->Form->input('User.curriculum.remove', array('type' => 'checkbox', 'label' => __('eliminar mi currículum actual', true))); ?>
					<?php echo $this->Form->end(__('Enviar', true)); ?>
					<p><small><strong>Puedes enviar estos tipos de archivo: <br/>.pdf, .odt, .doc o .docx.</strong></small></p>
				</fieldset>
			</div>
			<script type="text/javascript">

				$(document).ready(function(){
					$("#addUserCurriculumInPlace").click(function(){
						if($("#userCurriculumAdd").hasClass('open')){					
							$("#userCurriculumAdd").slideUp('fast');
						}else{
							$("#userCurriculumAdd").slideDown('fast');
						}
						$("#addUserCurriculumInPlace").toggleClass('open');
						$("#userCurriculumAdd").toggleClass('open');
						return false;
					});
				});

			</script>
		</li>
	</ul>
	
	<p class="userCurriculum">
		<strong><?php echo String::insert(__('Currículum de :userName', true),array('userName' => $user['User']['fullname'])); ?></strong>:
		<?php if($user['User']['curriculum']): ?>
			<?php echo $this->Html->link('Descargar', $this->Html->url(DS.$user['User']['curriculum_dir'].DS.$user['User']['curriculum'], true)); ?>
		<?php else: ?>
			No se ha añadido un currículum
		<?php endif; ?>
	</p>
	
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php __('Área'); ?></th>
		<th><?php __('Nivel'); ?></th>
		<th><?php __('Nombre del Título'); ?></th>
		<th><?php __('Validación');?></th>
		<th><?php __('Documento'); ?></th>
		<th><?php __('Referencia'); ?></th>
		<th class="actions"><?php __('Acciones');?></th>
	</tr>
	<?php
	$i = 0;
	$class = null;
	foreach ($user['UserKnowledge'] as $userKnowledge):
		$class = ($class == null)?' class="altrow"':null;
	?>
	<tr<?php echo $class;?>>
		<td>
			<strong><?php echo $userKnowledge['KnowledgeArea']['name']; ?></strong>
		</td>
		<td>
			<strong><?php echo $userKnowledge['KnowledgeLevel']['name']; ?></strong>
		</td>
		<td><?php echo $userKnowledge['name']; ?></td>
		<td><?php echo ($userKnowledge['validated'])?__('Validado',true):__('Sin Validar',true); ?></td>
		<td><?php if(!empty($userKnowledge['file'])) echo $this->Html->link(__('descargar', true), '/uploads/user_knowledge/file/'.$userKnowledge['file']); ?></td>
		<td>
			<?php if(!empty($userKnowledge['reference'])): ?>
			<?php echo $this->Html->link(__('ver', true), '#', array('class' => 'viewReference')); ?>
			<div class="userKnowledgeReference modalContent">
				<h4>Referencia</h4>
				<?php
					$userKnowledge['reference'] = $this->Text->autoLink($userKnowledge['reference']);
					$userKnowledge['reference'] = nl2br($userKnowledge['reference']);
					echo $this->Html->para(null,$userKnowledge['reference']);
				?>
			</div>
			<?php endif; ?>
		</td>
		<td class="actions">
			<?php
				if($userKnowledge['validated']){
					echo $this->Html->link(__('Invalidar', true), array('controller' => 'user_knowledges', 'action' => 'validation', 'opt' => false, $userKnowledge['id']), array('class' => 'button invalidate'));
				}else{
					echo $this->Html->link(__('Validar', true), array('controller' => 'user_knowledges', 'action' => 'validation', 'opt' => true, $userKnowledge['id']), array('class' => 'button validate'));
				}
			?>
			<?php echo $this->Html->link(__('Modificar', true), array('controller' => 'user_knowledges', 'action' => 'edit', $userKnowledge['id']), array('class' => 'button edit')); ?>
			<?php echo $this->Html->link(__('Eliminar', true), array('controller' => 'user_knowledges', 'action' => 'delete', $userKnowledge['id']), array('class' => 'button delete'), sprintf(__("¿Estás seguro que quieres eliminar \"%s\"?", true), $userKnowledge['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>

	<script type="text/javascript">

		$(document).ready(function(){
			$("a.viewReference").click(function(){
				$(this).parent().children('div.userKnowledgeReference').modal({
					overlayClose:true,
					closeClass: "modalClose",
					closeHTML: "<a href='#'>Cerrar</a>"
				});
				return false;
			});
		});	

	</script>

</div>