<div class="users form">

	<h2>Nuevo Voluntario</h2>
	<ul class="viewTools">
			<li><?php echo $this->Html->link('ver voluntarios', array('action' => 'index'), array('class' => 'button index')); ?></li>
	</ul>

<?php echo $this->Form->create('User');?>
	<fieldset class="personal">
 		<legend><?php __('Información Personal'); ?></legend>
	<?php
		echo $this->Form->input('firstname',array('type' => 'text', 'label' => 'Nombre'));
		echo $this->Form->input('lastname',array('type' => 'text', 'label' => 'Apellido'));
		echo $this->Form->input('secondary_lastname',array('type' => 'text', 'label' => 'Segundo Apellido'));

		echo $this->Form->input('id_type', array('label' => 'Tipo de Identificación'));
		echo $this->Form->input('id_number', array('label' => 'Número de Identificación'));

		if(isset($displayForceIdNumber) && $displayForceIdNumber){
			echo $this->Form->input('forceDuplicateIdNumber', array('type' => 'checkbox', 'label' => 'Si estás seguro(a) que quieres registrar otra cuenta con este RUT, selecciona esta opción.'));
		}
	?>
	</fieldset>

	<fieldset class="access">
 		<legend><?php __('Datos de Ingreso'); ?></legend>
	<?php
		echo $this->Form->input('mail');
		echo $this->Form->input('password');
		echo $this->Form->input('password_check',array('type' => 'password', 'label' => 'Confirma tu Password'));
	?>
	</fieldset>
	<fieldset class="contact">
 		<legend><?php __('Información de Contacto'); ?></legend>
	<?php
		echo $this->Form->input('location_id', array('empty' => '(seleccione una opción)'));
		echo $this->Form->input('city');
		echo $this->Form->input('address');
		echo $this->Form->input('phone');
	?>

	</fieldset>

<?php echo $this->Form->end(__('Crear', true));?>
</div>