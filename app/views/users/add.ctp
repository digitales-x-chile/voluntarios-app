<div id="welcome">

	<h2>Bienvenido a nuestro <br/> Portal de Voluntarios.</h2>
	
	<p class="registerArrow">Si eres nuevo o nueva aquí, te invitamos a registrarte y a participar con nosotros, utilizando el formulario de esta página. ¡Miles de familias te necesitan!</p>
	
	<p>Si ya estas registrado, <?php echo $html->link(__('ingresa con tu e-mail y contraseña aquí',true),array('controller' => 'users', 'action' => 'login')); ?>; podrás actualizar tus datos y revisar tu historial.</p>
	
	<p>Si no recuerdas tu constraseña, <?php echo $html->link(__('recuperala aquí',true),array('controller' => 'users', 'action' => 'recover')); ?>.</p>
	
	<p><strong>Recuerda completar tu perfil después de registrarte. De esta manera nos ayudarás a encontrar de mejor manera los voluntarios más capacitados para cada operativo o acción.</strong></p>
	
</div>


<div class="register form">

	<h2>Regístrate como Voluntario ahora</h2>
<?php echo $this->Form->create('User');?>
	<fieldset class="personal">
 		<legend><?php __('Información Personal'); ?></legend>
	<?php
		echo $this->Form->input('firstname',array('type' => 'text', 'label' => 'Nombre'));
		echo $this->Form->input('lastname',array('type' => 'text', 'label' => 'Apellido'));
		echo $this->Form->input('secondary_lastname',array('type' => 'text', 'label' => 'Segundo Apellido'));

		echo $this->Form->input('id_type', array('label' => 'Tipo de Identificación'));		
		echo $this->Form->input('id_number', array('label' => 'Número de Identificación'));
		
		if(isset($displayForceIdNumber) && $displayForceIdNumber){
			echo $this->Form->input('forceDuplicateIdNumber', array('type' => 'checkbox', 'label' => 'Si estás seguro(a) que quieres registrar otra cuenta con este RUT, selecciona esta opción.'));
		}
		
	?>
	</fieldset>
	<fieldset class="access">
 		<legend><?php __('Datos de Ingreso'); ?></legend>
	<?php
		echo $this->Form->input('mail');
		echo $this->Form->input('password');
		echo $this->Form->input('password_check',array('type' => 'password', 'label' => 'Confirma tu Password'));
	?>
	</fieldset>
	
<?php echo $this->Form->end(__('Regístrate', true));?>
</div>