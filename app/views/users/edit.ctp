<div class="users form">

	<h2>Editar mis Datos</h2>
	<ul class="viewTools">
			<li><?php echo $this->Html->link('volver a mi perfil', array('action' => 'me'), array('class' => 'button profile')); ?></li>
	</ul>


<?php echo $this->Form->create('User');?>
	<fieldset class="personal">
 		<legend><?php __('Información Personal'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('firstname',array('type' => 'text', 'label' => 'Nombre'));
		echo $this->Form->input('lastname',array('type' => 'text', 'label' => 'Apellido'));
		echo $this->Form->input('secondary_lastname',array('type' => 'text', 'label' => 'Segundo Apellido'));

		if(isset($displayForceIdNumber) && $displayForceIdNumber){
			echo $this->Form->input('forceDuplicateIdNumber', array('type' => 'checkbox', 'label' => 'Si estás seguro(a) que quieres registrar otra cuenta con este RUT, selecciona esta opción.'));
		}
	?>
	</fieldset>
	<fieldset class="access">
 		<legend><?php __('Datos de Ingreso'); ?></legend>
	<?php
		echo $this->Form->input('mail');
		echo $this->Form->input('password');
		echo $this->Form->input('password_check',array('type' => 'password', 'label' => 'Confirma tu Password'));
	?>

	</fieldset>
	
	<fieldset class="contact">
 		<legend><?php __('Información de Contacto'); ?></legend>
	<?php
		echo $this->Form->input('location_id');
		echo $this->Form->input('city');
		echo $this->Form->input('address');
		echo $this->Form->input('phone');
	?>

	</fieldset>

<?php echo $this->Form->end(__('Modificar', true));?>
</div>