<div class="userKnowledges form">
	<h2><?php __('Añadir Área de Conocimiento a Usuario');?></h2>

	<ul class="viewTools">
			<?php if(isset($user_id)): ?>
			<li><?php echo $this->Html->link('volver a mi perfil', array('controller' => 'users', 'action' => 'me'), array('class' => 'button index')); ?></li>
			<?php endif; ?>
	</ul>

<?php echo $this->Form->create('UserKnowledge', array('type' => 'file'));?>

 		
	<p class="important">
		<strong>Recuerda que una vez que tu Área de Conocimiento sea Validada, ésta no podrá ser modificada.</strong><br/><br/>
		<strong>Cada Nivel de Conocimiento puede requerir o no validación. Procura completar los datos.</strong>
	</p> 		

	<fieldset>
 		<legend><?php __('Añadir Área de Conocimiento a Usuario'); ?></legend>
 		
	<?php
		echo $this->Form->input('knowledge_area_id', array('label' => 'Área de Conocimiento'));
		echo $this->Form->input('knowledge_level_id', array('label' => 'Nivel de Conocimiento'));
		echo $this->Form->input('name', array('label' => 'Nombre del Título'));
		echo $this->Form->input('file', array('type' => 'file', 'label' => 'Documento de Validación'));
		echo $this->Form->input('reference', array('label' => 'Referencia'));
	?>
	<small>No es necesario que aquí te extiendas, una pequeña referencia es suficiente en caso de no existir un documento que acredite el área que quieres añadir.</small>
	</fieldset>
<?php echo $this->Form->end(__('Añadir', true));?>
</div>