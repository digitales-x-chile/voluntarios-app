<div class="userKnowledges view">
<h2><?php  __('User Knowledge');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userKnowledge['UserKnowledge']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('User'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($userKnowledge['User']['name'], array('controller' => 'users', 'action' => 'view', $userKnowledge['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Knowledge Area'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($userKnowledge['KnowledgeArea']['name'], array('controller' => 'knowledge_areas', 'action' => 'view', $userKnowledge['KnowledgeArea']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Knowledge Level'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($userKnowledge['KnowledgeLevel']['name'], array('controller' => 'knowledge_levels', 'action' => 'view', $userKnowledge['KnowledgeLevel']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userKnowledge['UserKnowledge']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Validated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userKnowledge['UserKnowledge']['validated']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('File'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userKnowledge['UserKnowledge']['file']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Reference'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userKnowledge['UserKnowledge']['reference']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User Knowledge', true), array('action' => 'edit', $userKnowledge['UserKnowledge']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete User Knowledge', true), array('action' => 'delete', $userKnowledge['UserKnowledge']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $userKnowledge['UserKnowledge']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List User Knowledges', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Knowledge', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Knowledge Areas', true), array('controller' => 'knowledge_areas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Knowledge Area', true), array('controller' => 'knowledge_areas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Knowledge Levels', true), array('controller' => 'knowledge_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Knowledge Level', true), array('controller' => 'knowledge_levels', 'action' => 'add')); ?> </li>
	</ul>
</div>
