<div class="userKnowledges index">
	<h2><?php __('Areas de Conocimiento sin Validadas');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php __('Usuario'); ?></th>
		<th><?php __('Área'); ?></th>
		<th><?php __('Nivel'); ?></th>
		<th><?php __('Nombre del Título'); ?></th>
		<th><?php __('Documento'); ?></th>
		<th><?php __('Referencia'); ?></th>
		<th class="actions"><?php __('Acciones');?></th>
	</tr>
	<?php
	$i = 0;
	$class = null;
	foreach ($userKnowledges as $userKnowledge):
		$class = ($class == null)?' class="altrow"':null;
	?>
	<tr<?php echo $class;?>>
		<td>
			<strong><?php echo $this->Html->link($userKnowledge['User']['fullname'], array('controller' => 'users', 'action' => 'view', $userKnowledge['User']['id'])); ?></strong>
		</td>
		<td>
			<strong><?php echo $userKnowledge['KnowledgeArea']['name']; ?></strong>
		</td>
		<td>
			<strong><?php echo $userKnowledge['KnowledgeLevel']['name']; ?></strong>
		</td>
		<td><?php echo $userKnowledge['UserKnowledge']['name']; ?></td>
		<td><?php if(!empty($userKnowledge['UserKnowledge']['file'])) echo $this->Html->link(__('descargar', true), '/uploads/user_knowledge/file/'.$userKnowledge['UserKnowledge']['file']); ?></td>
		<td>
			<?php if(!empty($userKnowledge['UserKnowledge']['reference'])): ?>
			<?php echo $this->Html->link(__('ver', true), '#', array('class' => 'viewReference')); ?>
			<div class="userKnowledgeReference modalContent">
				<h4>Referencia</h4>
				<?php
					$userKnowledge['UserKnowledge']['reference'] = $this->Text->autoLink($userKnowledge['UserKnowledge']['reference']);
					$userKnowledge['UserKnowledge']['reference'] = nl2br($userKnowledge['UserKnowledge']['reference']);
					echo $this->Html->para(null,$userKnowledge['UserKnowledge']['reference']);
				?>
			</div>
			<?php endif; ?>
		</td>
		<td class="actions">
			<?php
				if($userKnowledge['UserKnowledge']['validated']){
					echo $this->Html->link(__('Invalidar', true), array('controller' => 'user_knowledges', 'action' => 'validation', 'opt' => false, $userKnowledge['UserKnowledge']['id']), array('class' => 'button invalidate'));
				}else{
					echo $this->Html->link(__('Validar', true), array('controller' => 'user_knowledges', 'action' => 'validation', 'opt' => true, $userKnowledge['UserKnowledge']['id']), array('class' => 'button validate'));
				}
			?>
			<?php echo $this->Html->link(__('Modificar', true), array('controller' => 'user_knowledges', 'action' => 'edit', $userKnowledge['UserKnowledge']['id']), array('class' => 'button edit')); ?>
			<?php echo $this->Html->link(__('Eliminar', true), array('controller' => 'user_knowledges', 'action' => 'delete', $userKnowledge['UserKnowledge']['id']), array('class' => 'button delete'), sprintf(__("¿Estás seguro que quieres eliminar \"%s\"?", true), $userKnowledge['UserKnowledge']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>


	<script type="text/javascript">

		$(document).ready(function(){
			$("a.viewReference").click(function(){
				$(this).parent().children('div.userKnowledgeReference').modal({
					overlayClose:true,
					closeClass: "modalClose",
					closeHTML: "<a href='#'>Cerrar</a>"
				});
				return false;
			});
		});	

	</script>