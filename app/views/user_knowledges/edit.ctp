<div class="userKnowledges form">
	<h2><?php __('Modificar Área de Conocimiento de Usuario');?></h2>

	<ul class="viewTools">
			<li><?php echo $this->Html->link('volver a mi perfil', array('controller' => 'users', 'action' => 'me'), array('class' => 'button index')); ?></li>
	</ul>

<?php echo $this->Form->create('UserKnowledge', array('type' => 'file'));?>

	<p class="important">
		<strong>Recuerda que una vez que tu Área de Conocimiento sea Validada, ésta no podrá ser modificada.</strong>
	</p> 		
	<fieldset>
 		<legend><?php __('Modificar Área de Conocimiento de Usuario'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('knowledge_area_id', array('label' => 'Área de Conocimiento'));
		echo $this->Form->input('knowledge_level_id', array('label' => 'Nivel de Conocimiento'));
		echo $this->Form->input('name', array('label' => 'Nombre del Título'));
		echo $this->Form->input('file', array('type' => 'file', 'label' => 'Documento de Validación'));
		echo $this->Form->input('reference', array('label' => 'Referencia'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Modificar', true));?>
</div>