Hola <?php echo $name; ?>,

hemos recibido una petición para renovar tu contraseña, por lo que hemos hecho las gestiones necesarias y... TADAAAA!!!

Tu nueva contraseña es: <?php echo $password; ?>


Recuerda que para ingresar al sistema debes hacerlo con tu e-mail (<?php echo $mail; ?>) y la contraseña contenida en este correo, accediendo a <?php echo Configure::read('Voluntarios.appUrl'); ?>

Que tengas un buen día!

<?php echo Configure::read('Voluntarios.name'); ?>