Estimado/a <?php echo $name; ?>,

Has solicitado ser participante de la red de psicólogos más grandes del país. Para completar exitósamente el proceso de registro, ingresa al siguiente link, para que puedas participar en futuras intervenciones como parte de nuestro equipo:

<?php echo Configure::read('Voluntarios.appUrl').$html->url(array('controller' => 'users', 'action' => 'confirmation', 'hash' => $hash, 'id' => $id )); ?>

Como parte del equipo de Psicólogos Voluntarios de Chile, te doy la bienvenida a nuestro espacio. Este será tu email base para recibir nuestra información.

Para más información, escríbenos a comunicaciones@psicologosvoluntarios.cl

Muchas Gracias,
Psicólogos Voluntarios de Chile