<div id="menu" class="admin">
	<ul>
		<li><h4>Voluntarios</h4></li>
		<li><?php echo $html->link(__('Ver Todos',true),array('controller' => 'users', 'action' => 'index')); ?></li>
		<li><?php echo $html->link(__(' – Validar Conocimientos',true),array('controller' => 'user_knowledges', 'action' => 'index')); ?></li>
	</ul>
	<ul>
		<li><h4>Preferencias</h4></li>
		<li><?php echo $html->link(__('Áreas de Conocimiento',true),array('controller' => 'knowledge_areas', 'action' => 'index')); ?></li>
		<li><?php echo $html->link(__('Niveles de Conocimiento',true),array('controller' => 'knowledge_levels', 'action' => 'index')); ?></li>
		<li><?php echo $html->link(__('Localidades',true),array('controller' => 'locations', 'action' => 'index')); ?></li>
	</ul>
</div>