			<div id="header">
				<h1><a href="<?php echo Configure::read('Voluntarios.appUrl'); ?>"><span><?php echo Configure::read('Voluntarios.name'); ?></span></a></h1>
				<ul id="nav">
					<?php if (!$this->Session->read('Auth.User.id')): ?>
					<li class="register">
						<?php echo $html->link(__('Registro',true),array('controller' => 'users', 'action' => 'add', 'admin' => false)); ?>
					</li>
					<li class="login">
						<?php echo $html->link(__('Ingresa a tu Cuenta',true),array('controller' => 'users', 'action' => 'login', 'admin' => false)); ?>
					</li>
					<?php else: ?>
					<li class="account">
						¡<strong>Hola</strong> <?php echo $html->link($this->Session->read('Auth.User.name'), array('controller' => 'users', 'action' => 'me', 'admin' => false)); ?>!
					</li>
					<?php if($this->Session->read('Auth.User.admin')): ?>
					<li class="admin">
						<?php echo $html->link(__('Administración',true),array('controller' => 'users', 'action' => 'index', 'admin' => true)); ?>
					</li>
					<?php endif; ?>
					<li class="logout">
						<?php echo $html->link(__('Salir',true),array('controller' => 'users', 'action' => 'logout')); ?>
					</li>
					<?php endif; ?>
				</ul>
			</div>